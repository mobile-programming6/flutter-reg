import 'package:device_preview/device_preview.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart'; 
import 'main.dart';

class login extends StatefulWidget{
  @override
  _login createState() =>  _login();
}

class _login extends State<login>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 254, 243, 203),
        // leading:  IconButton(
        //   icon: Icon(Icons.sort),
        //   onPressed: (){
        //    Navigator.pop(context);
        //   }, 
        // ),
        // title: Text('Login',style: TextStyle(fontSize: 20 ,color: Color.fromARGB(255, 0, 0, 0))),
      // ), 
      body: login()
      // ListView(
      //   children: <Widget>[
      //     Column(
      //       children:[  
      //         Stack(
      //          children: [
      //           bg(),
      //           bg2(),
      //           loginText(),
      //          ]
      //         ),
      //       ]
      //     )
          
      //   ],
      // ), 
    );
 } 

 Widget login2(){
  return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 100.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 150, 
                    child: Image(image: AssetImage('assets/images/buuLogo.png')),
                    
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left:15.0,right: 15.0,top:40,bottom: 0), 
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email id as abc@gmail.com'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password'),
              ),
            ), 
            Container( 
                padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                child: SizedBox(
                  width: 250,
                  height: 50,
                  child: ElevatedButton(
                    child: const Text('Login'),
                    style: ElevatedButton.styleFrom(
                      elevation: 1,
                      backgroundColor: Color.fromARGB(255, 232, 217, 90),
                    ),
                    onPressed: () {
                      Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyApp()),
                      );
                    },
                  ),
                )
            ),
            SizedBox(
              height: 130,
            ),
            
          ],
        ),
      );
 }
//  Widget bg2() {
//   return Row(
//     children:[ 
//       SizedBox(width: 35,),
//       Column(
//       children: [
//         SizedBox(height: 70,),
//         Container( 
//                 width: 320,
//                 height: 550, 
//                 decoration: BoxDecoration(
//                 color: Color.fromARGB(255, 255, 255, 255),
//                 borderRadius: BorderRadius.only(
//                         bottomLeft: Radius.circular(40),
//                         bottomRight: Radius.circular(40),
//                         topLeft: Radius.circular(40),
//                         topRight: Radius.circular(40),
//                 ),
//               ), 
//               ),
//       ]
//     ),],
//   );
//  }

//  Widget bg() {
//   return Container(
    
//             width: 400,
//             height: 500, 
//             decoration: BoxDecoration(
//             color: Color.fromARGB(255, 232, 217, 90),
//             borderRadius: BorderRadius.only(
//                     bottomLeft: Radius.circular(20),
//                     bottomRight: Radius.circular(20),
//             ),
//           ),
//             child: Column(
//               children: <Widget>[
//                 const SizedBox(
//                   height: 10,
//                 ),  
//                   SingleChildScrollView( 
          
//                   )
//               ],
//             ),
//           );
//  }
TextEditingController nameController = TextEditingController();
TextEditingController passwordController = TextEditingController();
  Widget login() {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(
                  height: 50,
        ),   
        Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 150, 
                    child: Image(image: AssetImage('assets/images/buuLogo.png')),
                    
                ),
              ),
        ), 
        const SizedBox(
         height: 30,
        ),  
        Container(
              padding: const EdgeInsets.all(10),
              child: SizedBox(
                width: 350,
                child: TextField(
                  controller: nameController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'User Name',
                  ),
                  
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: SizedBox(
                width: 350,
                child: TextField(
                  obscureText: true,
                  controller: passwordController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  ),
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                //forgot password screen
              },
              child: const Text('Forgot Password',),
            ),
            Container( 
                padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                child: SizedBox(
                  width: 250,
                  height: 40,
                  child: ElevatedButton(
                    child: const Text('Login', style: TextStyle(fontSize: 20),),
                    style: ElevatedButton.styleFrom(
                      elevation: 1,
                      backgroundColor: Color.fromARGB(255, 232, 217, 90),
                    ),
                    onPressed: () {
                      Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyApp()),
                      );
                    },
                  ),
                )
            ),
            Row(
              children: <Widget>[
                const Text('Does not have account?'),
                TextButton(
                  child: const Text(
                    'Sign in',
                    style: TextStyle(fontSize: 20),
                  ),
                  onPressed: () {
                    //signup screen
                  },
                )
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
      ],
    );
  }
}