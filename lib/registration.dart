import 'package:device_preview/device_preview.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class registration extends StatefulWidget{
  @override
  _registration createState() =>  _registration();
}

class _registration extends State<registration>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 232, 217, 90),
        leading:  IconButton(
          icon: Icon(Icons.sort),
          onPressed: (){
           Navigator.pop(context);
          }, 
        ),
        title: Text('ผลการลงทะเบียน',style: TextStyle(fontSize: 20 ,color: Color.fromARGB(255, 0, 0, 0))),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),  
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal, 
                  child: Column(
                  children: <Widget>[
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Text("ปีการศึกษา"),
                        const SizedBox(width: 20),
                        DropdownYear()
                      ],
                    ),Padding(padding: EdgeInsets.all(5)),
                    Row(
                      children: [
                        Text("ภาคเรียนที่"),
                        const SizedBox(width: 20),
                        DropdownSemester()
                      ],
                    ),Padding(padding: EdgeInsets.all(10)),
                    subjectWidget1(), 
                    Padding(padding: EdgeInsets.all(10)),
                    subjectWidget2(),
                    Padding(padding: EdgeInsets.all(10)),
                    subjectWidget3(),
                    Padding(padding: EdgeInsets.all(10)),
                    subjectWidget4(),
                    Padding(padding: EdgeInsets.all(20)),
                  ]
                ),
               )
            ],
          ),
          
        ],
      ), 
    );
 }
Widget DropdownYear() {
  return CustomDropdownButton2(
        hint: '2565', 
        dropdownItems: year,
        value: selectedValue,
        onChanged: (value) {
          setState(() {
            selectedValue = value;
          });
        },
      );
}
Widget DropdownSemester() {
  return CustomDropdownButton2(
        hint: '2', 
        dropdownItems: Semester,
        value: selectedSemester,
        onChanged: (value) {
          setState(() {
            selectedSemester = value;
          });
        },
      );
} 
final List<String> Semester = [
  '1',
  '2',
];
String? selectedSemester;

final List<String> year = [
  '2563',
  '2564',
  '2565',
  '2566'
];
String? selectedValue;

Widget subjectWidget4() {
  return Container(          
          width: 320.0,
          height: 210.0,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 217, 215, 210),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 172, 171, 171),
                blurRadius: 10,
                spreadRadius: 0,
                offset: Offset(2,4)
              )
            ]
          ),
          
          child: Column(
            children: [
              Row( 
                children: [
                Text("รหัสวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                SizedBox(width: 80),
                Text("88624359"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row( 
                children: [
                Text("ชื่อรายวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 60),
                Text("Web Programming"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("แบบการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 40),
                Text("GD"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
              children: [
                Text("หน่วยกิต",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 80),
                Text("3"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("กลุ่ม",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 110),
                Text("1"),
              ]
              ), 
              
              ],
          ),  
  );
}

Widget subjectWidget2() {
  return Container(          
          width: 320.0,
          height: 210.0,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 217, 215, 210),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 172, 171, 171),
                blurRadius: 10,
                spreadRadius: 0,
                offset: Offset(2,4)
              )
            ]
          ),
          
          child: Column(
            children: [
              Row( 
                children: [
                Text("รหัสวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                SizedBox(width: 80),
                Text("79322263"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row( 
                children: [
                Text("ชื่อรายวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 60),
                Text("Cosmetic and Beauty"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("แบบการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 40),
                Text("GD"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
              children: [
                Text("หน่วยกิต",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 80),
                Text("2"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("กลุ่ม",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 110),
                Text("1"),
              ]
              ), 
              
              ],
          ),  
  );
}

Widget subjectWidget3() {
  return Container(          
          width: 320.0,
          height: 210.0,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 217, 215, 210),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 172, 171, 171),
                blurRadius: 10,
                spreadRadius: 0,
                offset: Offset(2,4)
              )
            ]
          ),
          
          child: Column(
            children: [
              Row( 
                children: [
                Text("รหัสวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                SizedBox(width: 90),
                Text("88624559"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row( 
                children: [
                Text("ชื่อรายวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 70),
                Text("Software Testing"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("แบบการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 50),
                Text("GD"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
              children: [
                Text("หน่วยกิต",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 90),
                Text("3"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("กลุ่ม",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 120),
                Text("1"),
              ]
              ), 
              
              ],
          ),  
  );
}

Widget subjectWidget1() {
  return Container(          
          width: 320.0,
          height: 230.0,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 217, 215, 210),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 172, 171, 171),
                blurRadius: 10,
                spreadRadius: 0,
                offset: Offset(2,4)
              )
            ]
          ),
          
          child: Column(
            children: [
              Row( 
                children: [
                Text("รหัสวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                SizedBox(width: 90),
                Text("88634459"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row( 
                children: [
                Text("ชื่อรายวิชา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 70),
                Text("Mobile Application \nDevelopment I"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("แบบการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 50),
                Text("GD"),
              ]
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
              children: [
                Text("หน่วยกิต",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 90),
                Text("3"),
              ] 
              ),
              Divider(color: Color.fromARGB(255, 255, 255, 255),),
            Row(
                children: [
                Text("กลุ่ม",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,)),
                SizedBox(width: 120),
                Text("1"),
              ]
              ), 
              
              ],
          ),  
  );
}

 Widget dataTable() {
    return Column(
      children: [
        DataTable( 
          columns: [
            DataColumn(label: Text('รหัสวิชา')),
            DataColumn(label: Text('88624359')), 
          ], 
          rows: [
            DataRow(cells: [
              DataCell(Text('ชื่อรายวิชา'),),
              DataCell(Text('Web Programming'),), 
            ]),
            DataRow(cells: [
              DataCell(Text('แบบการศึกษา'),),
              DataCell(Text('GD'),), 
            ]),
            DataRow(cells: [
              DataCell(Text('หน่วยกิต'),),
              DataCell(Text('3'),), 
            ]),
            DataRow(cells: [
              DataCell(Text('กลุ่ม'),),
              DataCell(Text('1'),), 
            ]), 
          ])
      ],
    );
  }
}