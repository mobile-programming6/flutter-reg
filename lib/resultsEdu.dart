import 'package:device_preview/device_preview.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class resultsEdu extends StatefulWidget{
  @override
  _resultEdu createState() =>  _resultEdu();
}

class _resultEdu extends State<resultsEdu>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 232, 217, 90),
        leading:  IconButton(
          icon: Icon(Icons.sort),
          onPressed: (){
           Navigator.pop(context);
          }, 
        ),
        title: Text('ผลการศึกษา',style: TextStyle(fontSize: 20 ,color: Color.fromARGB(255, 0, 0, 0))),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              const SizedBox(
                height: 25,
              ),  
                SingleChildScrollView(
                  child: Column(
                    children: [ 
                      SingleChildScrollView(
                        child: Row(
                          children: [
                            Padding(padding: EdgeInsets.all(10)),
                            Text("แสดงข้อมูล"),
                            const SizedBox(width: 20),
                            DropdownYear(), 
                          ],
                        ),
                      ),Padding(padding: EdgeInsets.all(10)),
                      Image(image: AssetImage('assets/images/เกรด1.jpg'),width:500),
                       Image(image: AssetImage('assets/images/เกรด2.jpg'),width:500),
                       const SizedBox(height: 30),
                    ],
                  ),

               )
            ],
          ),
          
        ],
      ), 
    );
 }
  Widget dataTable() {
    return Column(
      children: [
        Container(
          width: 360.0,
          height: 260.0,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 217, 215, 210),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 172, 171, 171),
                blurRadius: 10,
                spreadRadius: 0,
                offset: Offset(2,4)
              )
            ]
          ),
          child: DataTable(  
            columns: [
              DataColumn(
                label: Text('ชื่อ',style: TextStyle(fontStyle: FontStyle.italic),
                )
                ),
              DataColumn(label: Text('ซาเนีย หวังใจ')), 
            ], 
            rows: [

              DataRow(cells: [
                DataCell(Text('สถานภาพ',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('กำลังศึกษา'),), 
              ]),
              DataRow(cells: [
                DataCell(Text('คณะ',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('คณะวิทยาการสารสนเทศ'),), 
              ]),
              DataRow(cells: [
                DataCell(Text('หลักสูตร',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('วท.บ. ปรับปรุง59 - ป.ตรี\n4 ปี ปกติ'),), 
              ]),
              DataRow(cells: [
                DataCell(Text('อ. ที่ปรึกษา',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('อาจารย์ภูสิต กุลเกษม,\nผศ.ดร.โกเมศ อัมพวัน'),), 
              ]),  
            ]),
        )
      ],
    );
  }
  Widget DropdownYear() {
  return CustomDropdownButton2(
        hint: 'ทั้งหมด', 
        dropdownItems: year,
        value: selectedValue,
        onChanged: (value) {
          setState(() {
            selectedValue = value;
          });
        },
      );
}

final List<String> year = [
  'ทั้งหมด',
  '2563/1',
  '2563/2',
  '2564/1',
  '2564/2',
  '2565/1',
  '2565/2', 
];
String? selectedValue;
}
