import 'package:device_preview/device_preview.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';


class personal extends StatefulWidget{
  @override
  _personal createState() =>  _personal();
}

class _personal extends State<personal>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 232, 217, 90),
        leading:  IconButton(
          icon: Icon(Icons.sort),
          onPressed: (){
           Navigator.pop(context);
          }, 
        ),
        title: Text('ประวัตินิสิต',style: TextStyle(fontSize: 20 ,color: Color.fromARGB(255, 0, 0, 0))),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(height: 30),
                personalWidget()
              ],
            ),
          )
          
        ],
      ), 
    );
 }

 Widget personalWidget(){
  return Column(
            children: [  
              Row( 
                children: [
                const SizedBox(width: 30),
                Image(image: AssetImage('assets/images/me.jpg'),width: 100),
                const SizedBox(width: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("นางสาวซาเนีย หวังใจ",style: TextStyle(fontSize: 20),), 
                    SizedBox(height:2),
                    Text("MISS SANIA WANGJAI",style: TextStyle(fontSize: 20),),
                    SizedBox(height:10),
                    Text("รหัสประจำตัว:	63160191 ",style: TextStyle(fontSize: 15),),
                  ],
                )
              ]
              ),SizedBox(height:15),
              Container(
                width: 340,
                height: 25,
                color: Color.fromARGB(255, 203, 170, 213),
                child: Row(
                  children: [
                    SizedBox(width:10),
                    Text("ข้อมูลด้านการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                
                  ]
                ),
              ), SizedBox(height:10),
            Row( 
                children: [
                SizedBox(width:38),
                Text("คณะ:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 125),
                Text("คณะวิทยาการสารสนเทศ"),
              ]
              ),SizedBox(height:5),
            Row( 
                children: [
                SizedBox(width:38),
                Text("วิทยาเขต:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 95),
                Text("บางแสน"),
              ]
              ),SizedBox(height:5),
              Row( 
                
                children: [
                SizedBox(width:38),
                Text("หลักสูตร:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 100),
                Text("2115020 วท.บ. 4 ปี ปกติ"),
              ]
              ),SizedBox(height:5),
              Row( 
                children: [
                SizedBox(width:38),
                Text("ระดับการศึกษา",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width:63),
                Text("ปริญญาตรี"),
              ]
              ),SizedBox(height:5),
              Row( 
                children: [
                SizedBox(width:38),
                Text("ชื่อปริญญา:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width:85),
                Text("วิทยาศาสตรบัณฑิต วท.บ."),
              ]
              ),SizedBox(height:5),
              Row( 
                children: [
                SizedBox(width:38),
                Text("ปีการศึกษาที่เข้า:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 50),
                Text("2563 / 1 วันที่ 21/5/2563"),
              ]
              ),SizedBox(height:5),
              Row( 
                children: [
                SizedBox(width:38),
                Text("วิธีรับเข้า:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 100),
                Text("รับตรงทั่วประเทศ"),
              ]
              ),SizedBox(height:5),
              Row( 
                children: [
                SizedBox(width:38),
                Text("วุฒิก่อนเข้ารับการศึกษา:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width:7),
                Text("ม.6"),
              ]
              ),
               SizedBox(height:15),
              Container(
                width: 340,
                height: 25,
                color: Color.fromARGB(255, 179, 170, 213),
                child: Row(
                  children: [
                    SizedBox(width:10),
                    Text("ผลการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                
                  ]
                ),
              ), SizedBox(height:10),
            
              Row( 
                children: [
                SizedBox(width:38),
                Text("หน่วยกิตคำนวณ:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 60),
                Text("91"),
              ]
              ),SizedBox(height:5),
            Row( 
                children: [
                SizedBox(width:38),
                Text("หน่วยกิตที่ผ่าน:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 70),
                Text("91"),
              ]
              ),SizedBox(height:5),
              Row( 
                
                children: [
                SizedBox(width:38),
                Text("คะแนนเฉลี่ยสะสม:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 50),
                Text("3.45"),
              ]
              ),
              SizedBox(height:15),
              Container(
                width: 340,
                height: 25,
                color: Color.fromARGB(255, 170, 196, 213),
                child: Row(
                  children: [
                    SizedBox(width:10),
                    Text("ข้อมูลส่วนบุคคล",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                
                  ]
                ),
              ), SizedBox(height:10),
            Row( 
                children: [
                SizedBox(width:38),
                Text("สัญชาติ:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width: 110),
                Text("ไทย"),
              ]
              ),SizedBox(height:5),
              Row( 
                children: [
                SizedBox(width:38),
                Text("ศาสนา:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width:118),
                Text("อิสลาม"),
              ]
              ),SizedBox(height:5),
              Row( 
                children: [
                SizedBox(width:38),
                Text("หมู่เลือด:",style: TextStyle(fontSize: 15 ,)),
                SizedBox(width:110),
                Text("B"),
              ]
              ),SizedBox(height: 30),
              ],
          ); 
  
 }
}