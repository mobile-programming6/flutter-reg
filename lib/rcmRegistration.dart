import 'package:device_preview/device_preview.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter_reg/registration.dart'; 
import 'main.dart';

class rcmRegistration extends StatefulWidget{
  @override
  _rcmRegistration createState() =>  _rcmRegistration();
}

class _rcmRegistration extends State<rcmRegistration>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 232, 217, 90),
        leading:  IconButton(
          icon: Icon(Icons.sort),
          onPressed: (){
           Navigator.pop(context);
          }, 
        ),
        title: Text('แนะนำการลงทะเบียน',style: TextStyle(fontSize: 20 ,color: Color.fromARGB(255, 0, 0, 0))),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Column(
            children:[
              SizedBox(height: 20,),
              Row( 
                  children: [
                    SizedBox(width: 15,),
                  Text("ขั้นตอนการลงทะเบียน" ,style: TextStyle(color: Colors.black,fontSize:20,fontWeight: FontWeight.bold),),
                ]
                ),
              
              SizedBox(height: 15,),
              registration()
            ]
          )
          
        ],
      ), 
    );
 }  
 Widget registration() {
  return SingleChildScrollView(
    child: Container(          
            width: 370.0,
            height: 970.0,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 217, 215, 210),
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(255, 172, 171, 171),
                  blurRadius: 10,
                  spreadRadius: 0,
                  offset: Offset(2,4)
                )
              ]
            ),
            
            child: Column(
              children: [
                Row( 
                  children: [
                  Text("1.ลงทะเบียน",style: TextStyle(color: Color.fromARGB(255, 224, 1, 1),fontSize: 20,decoration: TextDecoration.underline,decorationColor: Color.fromARGB(255, 224, 1, 1)),),
                ]
                ),
                Row( 
                  children: [
                  Text("เมื่อนิสิตทำการ login และกรอกประวัติเข้ามาแล้ว\nเมื่ออยู่ในช่วงลงทะเบียนหรือเพิ่ม-ลดรายวิชา \nระบบจะเข้าไปในหน้าลงทะเบียนอัติโนมัติ" ,style: TextStyle(color: Colors.black,fontSize: 15),),
                ]
                ),
                Divider(color: Color.fromARGB(255, 255, 255, 255),),
              Row(
                  children: [
                  Text("1.1 ลงทะเบียนแบบปกติ" ,style: TextStyle(color: Color.fromARGB(255, 224, 1, 1),fontSize: 15,decoration: TextDecoration.underline,decorationColor: Color.fromARGB(255, 224, 1, 1),),),
                ]
                ),
              Row(
                children: [
                  Text("ใช้ในการลงทะเบียนทั่วไป ให้ป้อนรหัสวิชา \nและกลุ่มเรียนที่ต้องการลงทะเบียน แล้วกดค้นหา" ,style: TextStyle(color: Colors.black,fontSize: 15),),
                ]
                ),
                Image(image: AssetImage('assets/images/แนะนำการลงทะเบียน1.jpg'),width: 300),
                Row(
                children: [
                  Text("จากนั้นจะพบตระกร้าให้คลิกตามกลุ่มที่ต้องการ" ,style: TextStyle(color: Colors.black,fontSize: 15),),
                ]
                ),
                Image(image: AssetImage('assets/images/แนะนำการลงทะเบียน2.jpg'),width: 300),
                Row(
                children: [
                  Text("จะได้รายการดังรูป ทำจนครบตามจำนวนรายวิชาที่\nต้องการ หากมีรายการใดๆเกินมาที่ไม่ต้องการลง\nให้คลิกที่คำว่า 'ลบ'" ,style: TextStyle(color: Colors.black,fontSize: 15),),
                ]
                ),
                Image(image: AssetImage('assets/images/แนะนำการลงทะเบียน3.jpg'),width: 300),
                Row(
                  children: [
                  Text("1.2 ลงทะเบียนแบบ audit ให้ติดต่อทำคำร้อง \nRE01 ส่งที่กองทะเบียนฯ" ,style: TextStyle(color: Color.fromARGB(255, 224, 1, 1),fontSize: 15,decoration: TextDecoration.underline,decorationColor: Color.fromARGB(255, 224, 1, 1),),),
                ]
                ),
                Row(
                  children: [
                  Text("1.3 ลงทะเบียนแบบ thesis แบบแบ่งหน่วยกิต" ,style: TextStyle(color: Color.fromARGB(255, 224, 1, 1),fontSize: 15,decoration: TextDecoration.underline,decorationColor: Color.fromARGB(255, 224, 1, 1),),),
                ]
                ),Image(image: AssetImage('assets/images/แนะนำการลงทะเบียน4.jpg'),width: 300),
                Divider(color: Color.fromARGB(255, 255, 255, 255),),
              Row( 
                  children: [
                  Text("2.ขั้นตอนการยืนยันการลงทะเบียน",style: TextStyle(color: Color.fromARGB(255, 224, 1, 1),fontSize: 20,decoration: TextDecoration.underline,decorationColor: Color.fromARGB(255, 224, 1, 1)),),
                ]
                ),
                Image(image: AssetImage('assets/images/แนะนำการลงทะเบียน5.jpg'),width: 300),
                
                ],
            ),  
    ),
  );
}
}