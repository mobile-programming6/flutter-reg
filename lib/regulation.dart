import 'package:device_preview/device_preview.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter_reg/registration.dart'; 
import 'main.dart';

class regulation extends StatefulWidget{
  @override
  _regulation createState() =>  _regulation();
}

class _regulation extends State<regulation>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 232, 217, 90),
        leading:  IconButton(
          icon: Icon(Icons.sort),
          onPressed: (){
           Navigator.pop(context);
          }, 
        ),
        title: Text("ระเบียบข้อบังคับ",style: TextStyle(fontSize: 20 ,color: Color.fromARGB(255, 0, 0, 0))),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Column(
            children:[
              SizedBox(height: 20,),
              Row( 
                  children: [
                    SizedBox(width: 15,),
                  Text("ข้อบังคับ-มหาวิทยาลัยบูรพา" ,style: TextStyle(color: Colors.black,fontSize:20,fontWeight: FontWeight.bold),),
                ]
                ),
              
              SizedBox(height: 10,), 
              Image(image: AssetImage('assets/images/ระเบียบข้อบังคับ1.jpg'),width:400),
              Image(image: AssetImage('assets/images/ระเบียบข้อบังคับ2.jpg'),width:400),
            ]
          )
          
        ],
      ), 
    );
 }  
}