import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reg/login.dart';
import 'package:flutter_reg/rcmRegistration.dart';
import 'package:flutter_reg/registration.dart';
import 'package:flutter_reg/regulation.dart';
import 'drawerScreen.dart';

void main() {
  runApp(DevicePreview(
    enabled: true,
    builder: (BuildContext context) => const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorSchemeSeed: const Color(0xff6750a4), useMaterial3: true,
      ),
      home: const MyHomePage(title: 'REG.BUU'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
  
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
      // login()
      
      Stack(
        children: [
          drawerScreen(),
          homeBodyWidget(),
        ],
      ), 
    );
  }

double xOffset=0;
double yOffset=0;
double scaleFactor=1;

bool isDrawerOpen = false;

Widget homeBodyWidget(){ 
  return AnimatedContainer(
    transform: Matrix4.translationValues(xOffset, yOffset, 0)
    ..scale(scaleFactor)..rotateY(isDrawerOpen? -0.5:0),
    duration: Duration(microseconds: 250),
    decoration: BoxDecoration(
      color: Colors.grey[200],
      borderRadius: BorderRadius.circular(isDrawerOpen?40:0.0)
    ),
    child: ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            const SizedBox(
              height: 35,
            ), 
              Row( 
                children: <Widget>[ 
                  const SizedBox(
                    width: 10,
                  ), 
                  isDrawerOpen?IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    onPressed: () {
                      setState(() {
                      xOffset = 0;
                      yOffset =  0;
                      scaleFactor=1;
                      isDrawerOpen=false;
                    });
                    },
                  ): IconButton(
                  icon: Icon(Icons.sort),
                  onPressed: (){ 
                    setState(() {
                      xOffset = 240; //300
                      yOffset = 130;
                      scaleFactor=0.7;
                      isDrawerOpen=true;
                    });
                  },
                  ),
              SizedBox(width: 35,),
              Image.network('https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png',height: 55,),
              SizedBox(width: 5,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[ 
                  Text("REG.BUU",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold)),
                  Text("REGISTRAR DIVISION",)
                ],
              ),
              SizedBox(width: 50,),
              Icon(Icons.settings),
            ],
          ),
          ],),
        SingleChildScrollView(
          child: Column(
          children: <Widget>[
            const SizedBox(
              height: 40,
            ),
            newsTextWidget(),
            Divider(
              indent: 10,
              height: 30,
              thickness: 2,
              color: Color.fromARGB(255, 87, 80, 12),
              endIndent: 200,
            ),
            newsScrollWidget(),
            const SizedBox(
              height: 25,
            ),
            quickTextWidget(), 
            Divider(
              indent: 10,
              height: 30,
              thickness: 2,
              color: Color.fromARGB(255, 87, 80, 12),
              endIndent: 200,
            ),
            ButtonWidget(),
            const SizedBox(
              height: 15,
            ),
            ButtonWidget2(),
            const SizedBox(
              height: 25,
            ),
            linksTextWidget(), 
            Divider(
              indent: 10,
              height: 30,
              thickness: 2,
              color: Color.fromARGB(255, 87, 80, 12),
              endIndent: 200,
            ),
            ButtonLinkWidget(),
            const SizedBox(
              height: 40,
            ),
            Container(
              height: 200,
              color: Color.fromARGB(255, 124, 72, 142),
              child: Center(
                child: Column(children: <Widget>[
                  Container(padding: const EdgeInsets.all(4)),
                  Text("ติดต่อเรา", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.location_on,color: Colors.white,),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("169 ถนน ลงหาดบางแสน ตำบลแสนสุข", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
  
                    ]
                  ),
                  Text("อำเภอเมืองชลบุรี ชลบุรี 20131", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.email,color: Colors.white,),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("administrator@buu.ac.th", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
                    ]
                  ),
                  Text("ติดตามเรา", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.facebook,color: Colors.white,),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("Facebook", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
                    ]
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.web_asset,color: Colors.white,),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("Website", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
                    ]
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.forum ,color:Colors.white,),
                      Container(padding: const EdgeInsets.all(3)),
                      Text("Line", style: const TextStyle(fontSize: 15, color: Color.fromARGB(255, 255, 255, 255))),
                    ]
                  ),
                  
                ]),
              ),
            )
          ],
        ),
          
        ),
        
      ],
    ),
  );
}

// Widget BoxShadow(){
//   return BoxShadow( 
//   );
// }

Widget ButtonWidget2(){
  return Container( 
      child : Row( 
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(180, 90),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
              ),
              backgroundColor: Color.fromARGB(255, 232, 217, 90), 
              padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.book,size: 30,),
            label: Text('วิชาที่เปิดสอน',style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 54, 48, 6)))
          ),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(180, 90),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
              ),
              backgroundColor: Color.fromARGB(255, 232, 217, 90), 
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.school,size: 30,),  //icon data for elevated button
            label: Text('ระบบขอสำเร็จ\nการศึกษา',style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 54, 48, 6))),
                 
          ),
          
        ]
    ),
  ); 
}
Widget ButtonLinkWidget(){
  return Container( 
      child : Row( 
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(180, 90),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
              ),
              backgroundColor: Color.fromARGB(255, 232, 217, 90),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.people,size: 30,),
            label: Text('ระบบประเมิน\nการสอนอาจารย์',style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 54, 48, 6)))
          ),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(180, 90),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
              ),
              backgroundColor: Color.fromARGB(255, 232, 217, 90),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            ),
            onPressed: (){},
            icon: Icon(Icons.domain,size: 30,),  //icon data for elevated button
            label: Text('ระบบจองหอพักนักศึกษา',style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 54, 48, 6))),
                 
          ),
          
        ]
    ),
  ); 
}

Widget ButtonWidget(){
  return Container( 
      child : Row( 
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(180, 90),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
              ),
              backgroundColor: Color.fromARGB(255, 232, 217, 90),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => rcmRegistration()),
              );
            },
            icon: Icon(Icons.assignment,size: 30,),
            label: Text('แนะนำ\nการลงทะเบียน',style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 54, 48, 6)))
          ),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(180, 90),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
              ),
              backgroundColor: Color.fromARGB(255, 232, 217, 90),
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
            ),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => regulation()),
              ); 
            },
            icon: Icon(Icons.description,size: 30,),  //icon data for elevated button
            label: Text('ระเบียบข้อบังคับ',style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 54, 48, 6))),
                 
          ),
          
        ]
    ),
  ); 
}

Widget newsScrollWidget(){
  return SingleChildScrollView(
        scrollDirection: Axis.horizontal, 
        child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Image(image: AssetImage('assets/images/ข่าวสาร1.png'),width: 395),
          Image(image: AssetImage('assets/images/ข่าวสาร2.png'),width: 395),
          Image(image: AssetImage('assets/images/ข่าวสาร3.png'),width: 395),
          Image(image: AssetImage('assets/images/ข่าวสาร4.png'),width: 395),
          Image(image: AssetImage('assets/images/ข่าวสาร5.png'),width: 395),
          Image(image: AssetImage('assets/images/ข่าวสาร6.png'),width: 395),
        ]
        ),);
}


Widget newsScroll01Widget(){
  return SingleChildScrollView(
        scrollDirection: Axis.horizontal, 
        child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Image(image: AssetImage('assets/images/1.png'),width: 280),
          Image(image: AssetImage('assets/images/2.png'),width: 280),
          Image(image: AssetImage('assets/images/3.png'),width: 280),
          Image(image: AssetImage('assets/images/4.png'),width: 280),
        ]
        ),);
}
Widget linksTextWidget() {
  return SingleChildScrollView( 
        child: Row( 
          mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(padding: const EdgeInsets.all(5.0)),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 0), //apply padding horizontal or vertical only
            child: Text('Related Links',style: TextStyle( 
                  fontSize: 20,
                  height: 1.2, //line height 150% of actual height
                  color: Color.fromARGB(255, 54, 48, 6),
                  fontWeight: FontWeight.bold, 
              ),  
          ),
          ),
        
        ]
        ),);
}

Widget quickTextWidget() {
  return SingleChildScrollView( 
        child: Row( 
          mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(padding: const EdgeInsets.all(5.0)),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 0), //apply padding horizontal or vertical only
            child: Text('Quick Access',style: TextStyle( 
                  fontSize: 20,
                  height: 1.2, //line height 150% of actual height
                  color: Color.fromARGB(255, 54, 48, 6),
                  fontWeight: FontWeight.bold, 
              ),  
          ),
          ),
        
        ]
        ),);
}

Widget newsTextWidget() {
  return SingleChildScrollView( 
        child: Row( 
          mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(padding: const EdgeInsets.all(5.0)),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 0), //apply padding horizontal or vertical only
            child: Text("ข่าวประกาศ",style: TextStyle( 
                  fontSize: 20,
                  height: 1.2, //line height 150% of actual height
                  color: Color.fromARGB(255, 54, 48, 6),
                  fontWeight: FontWeight.bold, 
              ),  
          ),
          ),
        
        ]
        ),);
}


}
