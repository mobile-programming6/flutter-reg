import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reg/login.dart';
import 'package:flutter_reg/personal.dart';
import 'package:flutter_reg/schedule.dart';
import 'registration.dart';
import 'package:flutter_reg/resultsEdu.dart';

class drawerScreen extends StatefulWidget{
  @override
  _drawerScreen createState() =>  _drawerScreen();
}

class _drawerScreen extends State<drawerScreen>{ 
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black ,
      padding: EdgeInsets.only(top: 50,bottom: 30,left: 7),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SizedBox(width: 20,),
              CircleAvatar(
                backgroundImage: NetworkImage('https://scontent.fbkk20-1.fna.fbcdn.net/v/t39.30808-6/320696626_1025315541962123_9132755071426402390_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=cR6n-jOZuZwAX9KftDm&_nc_ht=scontent.fbkk20-1.fna&oh=00_AfC5xrAdWwNrUVbxGQJCaVm7DWla3IILdANJ6W-uIZd5ag&oe=63D9BF9F'),
                radius: 30,
              ),
              SizedBox(width: 10,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Sania Wangjai",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),),
                  Text("63160191",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 15),)
                ],
              )
            ],
          ),
        Column(
          children: menu.map((element) => Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
            children: [ 
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom( 
                elevation: 0,
                shape: RoundedRectangleBorder( 
                  borderRadius: BorderRadius.circular(0)
                ),
                backgroundColor: Colors.black87,
              ),
                onPressed: (){
                    if(menu.indexOf(element)==0){
                      showDialog(context: context,
                      builder: (context) => AlertDialog(
                      actions: [
                        TextButton(onPressed: (){
                          Navigator.pop(context);
                        }, child: Text('Close'))
                      ],
                      title: const Text('reg.buu'),
                      content: const Text('ยังไม่ถึงช่วงเวลาการลงทะเบียน'),
                    ));
                  }else if(menu.indexOf(element)==1){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => registration()),
                    );
                  }else if(menu.indexOf(element)==2){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => schedule()),
                    );
                  }else if(menu.indexOf(element)==3){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => personal()),
                    );
                  }else if(menu.indexOf(element)==4){
                    showDialog(context: context,
                      builder: (context) => AlertDialog(
                      actions: [
                        TextButton(onPressed: (){
                          Navigator.pop(context);
                        }, child: Text('Close'))
                      ],
                      title: const Text('reg.buu'),
                      content: const Text('ขณะนี้ยังไม่มีรายการค่าใช้จ่าย!'),
                    ));
                  }else if(menu.indexOf(element)==5){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => resultsEdu()),
                    );
                  }
                }, 
                icon: Icon(element['icon'], color: Colors.white,size: 30,),
                label: Text(element['title'],style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20))
                )
              ],
            ),
          )).toList(),
        
        ), 
        Row(
          children: [
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.black87,
              ),onPressed: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => login()),
                );
              },
              icon: Icon(Icons.door_back_door_outlined ,color: Colors.white,size: 30,),
              label:Text("Log out",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
            ),
          ],
        )
        ]
        ),
    );
  }
  List<Map> menu=[
    {
      'icon' : Icons.event_note,
      'title' : 'ลงทะเบียน' 
    },
    {
      'icon' : Icons.event_available,
      'title' : 'ผลการลงทะเบียน'
    },
    {
      'icon' : Icons.dashboard,
      'title' : 'ตารางเรียน/สอบ'
    },
    {
      'icon' : Icons.assignment_ind,
      'title' : 'ประวัตินิสิต'
    },
    {
      'icon' : Icons.money,
      'title' : 'ภาระค่าใช้จ่ายทุน'
    },
    {
      'icon' : Icons.import_contacts,
      'title' : 'ผลการศึกษา'
    },
  ];
  
  
}