import 'package:device_preview/device_preview.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class schedule extends StatefulWidget{
  @override
  _schedule createState() =>  _schedule();
}

class _schedule extends State<schedule>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 232, 217, 90),
        leading:  IconButton(
          icon: Icon(Icons.sort),
          onPressed: (){
           Navigator.pop(context);
          }, 
        ),
        title: Text('ตารางเรียน/สอบ',style: TextStyle(fontSize: 20 ,color: Color.fromARGB(255, 0, 0, 0))),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),  
                SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 20),
                      dataTable(),
                      SizedBox(height: 20), 
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(10)),
                          Text("ปีการศึกษา"),
                          const SizedBox(width: 20),
                          DropdownYear(), 
                        ],
                      ),Padding(padding: EdgeInsets.all(5)),
                      Row(
                        children: [ 
                          Padding(padding: EdgeInsets.all(10)),
                          Text("ภาคเรียนที่"),
                          const SizedBox(width: 20),
                          DropdownSemester()
                        ],
                      ),Padding(padding: EdgeInsets.all(10)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: 10,),
                          Text("ตารางเรียน", style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                        ],
                      ),
                      Padding(padding: EdgeInsets.all(5)),
                      Image(image: AssetImage('assets/images/ตารางเรียน.jpg'),width: 380),
                      Padding(padding: EdgeInsets.all(7)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: 10,),
                          Text("ตารางสอบ", style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                        ],
                      ),Padding(padding: EdgeInsets.all(3)),
                      Image(image: AssetImage('assets/images/ตารางสอบ.jpg'),width: 390), 
                      SizedBox(height: 40,),
                    ],
                  ),

               )
            ],
          ),
          
        ],
      ), 
    );
 }
  Widget dataTable() {
    return Column(
      children: [
        Container(
          width: 360.0,
          height: 260.0,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 217, 215, 210),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 172, 171, 171),
                blurRadius: 10,
                spreadRadius: 0,
                offset: Offset(2,4)
              )
            ]
          ),
          child: DataTable(  
            columns: [
              DataColumn(
                label: Text('ชื่อ',style: TextStyle(fontStyle: FontStyle.italic),
                )
                ),
              DataColumn(label: Text('ซาเนีย หวังใจ')), 
            ], 
            rows: [

              DataRow(cells: [
                DataCell(Text('สถานภาพ',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('กำลังศึกษา'),), 
              ]),
              DataRow(cells: [
                DataCell(Text('คณะ',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('คณะวิทยาการสารสนเทศ'),), 
              ]),
              DataRow(cells: [
                DataCell(Text('หลักสูตร',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('วท.บ. ปรับปรุง59 - ป.ตรี\n4 ปี ปกติ'),), 
              ]),
              DataRow(cells: [
                DataCell(Text('อ. ที่ปรึกษา',style: TextStyle(fontStyle: FontStyle.italic),),),
                DataCell(Text('อาจารย์ภูสิต กุลเกษม,\nผศ.ดร.โกเมศ อัมพวัน'),), 
              ]),  
            ]),
        )
      ],
    );
  }
  Widget DropdownYear() {
  return CustomDropdownButton2(
        hint: '2565', 
        dropdownItems: year,
        value: selectedValue,
        onChanged: (value) {
          setState(() {
            selectedValue = value;
          });
        },
      );
}
Widget DropdownSemester() {
  return CustomDropdownButton2(
        hint: '2', 
        dropdownItems: Semester,
        value: selectedSemester,
        onChanged: (value) {
          setState(() {
            selectedSemester = value;
          });
        },
      );
} 
final List<String> Semester = [
  '1',
  '2',
];
String? selectedSemester;

final List<String> year = [
  '2563',
  '2564',
  '2565',
  '2566'
];
String? selectedValue;
}
